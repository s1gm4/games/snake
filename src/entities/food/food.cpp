#include "./food.hpp"

Food::Food()
{
    *this = Food(0, 0);
}

Food::Food(int x_, int y_)
{
    x = x_;
    y = y_;
    size = 10;
}

Food Food::random()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> x_dist(-WIDTH / 2, WIDTH / 2);
    std::uniform_int_distribution<> y_dist(-HEIGHT / 2, HEIGHT / 2);
    return Food(x_dist(gen), y_dist(gen));
}

void Food::show()
{
    
    fill(1, 0, 0);
    rect(x, y, size, size);
}

int Food::get_x()
{
    return x;
}

int Food::get_y()
{
    return y;
}