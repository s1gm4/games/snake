#pragma once

#define NAME "Snake"
#define VERSION "0.1"
#define AUTHOR "Samuel ORTION"
#define LICENSE "GPL-3.0-or-later"
#define TITLE NAME " " VERSION
#define WIDTH 500
#define HEIGHT 500
#define FPS 30
