#include <iostream>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "./utils/graphics.hpp"
#include "./entities/snake/snake.hpp"
#include "./entities/food/food.hpp"

#define UNUSED(x) (void)(x)

Snake snake;
Food food;

void start()
{
    snake = Snake(0, 0);
    food = Food::random();
}

void update(int x)
{
    UNUSED(x);
    glClear(GL_COLOR_BUFFER_BIT);
    snake.update();
    food.show();
    if (snake.is_eating(food))
    {
        snake.grow();
        food = Food::random();
    }
    glFlush();
    glutTimerFunc(1000 / FPS, update, 0);
}

void display(void)
{
    glutTimerFunc(1000 / FPS, update, 0);
}

void process_SHIFT_ALT_CTRL(unsigned char key, int x, int y)
{
    UNUSED(x);
    UNUSED(y);
    if (key == 115)
    {
        start();
    }
    // Press ALT or  SHIFT or  CTRL in combination with other keys.
    printf("key_code =%d  \n", key);

    int mod = glutGetModifiers();

    if (mod != 0) // ALT=4  SHIFT=1  CTRL=2
    {
        switch (mod)
        {
        case 1:
            printf("SHIFT key %d\n", mod);
            break;
        case 2:
            printf("CTRL  key %d\n", mod);
            break;
        case 4:
            printf("ALT   key %d\n", mod);
            break;
            mod = 0;
        }
    }
}

void process_Normal_Keys(int key, int x, int y)
{
    UNUSED(x);
    UNUSED(y);

    switch (key)
    {
    case 27:
        break;
    case 100:
        snake.setDirection(Snake::LEFT);
        break;
    case 102:
        snake.setDirection(Snake::RIGHT);
        break;
    case 101:
        snake.setDirection(Snake::UP);
        break;
    case 103:
        snake.setDirection(Snake::DOWN);
        break;
    case 115:
        start();
        break;
    }
}

int main(int argc, char **argv)
{

    std::cout << "Starting " << TITLE << " by " << AUTHOR << " (" << LICENSE << ")" << std::endl;
    // Initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glPointSize(10.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glutInitWindowSize(WIDTH, HEIGHT);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(TITLE);
    glutDisplayFunc(display);
    gluOrtho2D(-WIDTH / 2, WIDTH / 2, -HEIGHT / 2, HEIGHT / 2);
    glutSpecialFunc(process_Normal_Keys);
    glutKeyboardFunc(process_SHIFT_ALT_CTRL);
    glutMainLoop();
}